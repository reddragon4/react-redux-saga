import { configureStore, combineReducers } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counterSlice";
import containerOneReducer from "../container/container-one/redux/containeroneslice";
import createSagaMiddleware from "redux-saga";
import { watcherSagaOne } from "../saga/containerOneSaga";
const sagaMiddleware = createSagaMiddleware();

const reducer = combineReducers({
  containerOne: containerOneReducer,
});
const store = configureStore({
  reducer,
  middleware: [sagaMiddleware],
});
sagaMiddleware.run(watcherSagaOne);
export default store;
