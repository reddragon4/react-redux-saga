import { takeLatest, takeEvery } from "redux-saga/effects";
import { ContainerOneHandler } from "../container/container-one/redux/containeronehandlers";
import { getServerData } from "../container/container-one/redux/containeroneslice";

export function* watcherSagaOne() {
  yield takeLatest(getServerData.type, ContainerOneHandler);
}
