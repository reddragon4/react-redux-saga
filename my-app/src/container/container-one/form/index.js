import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getServerData, setContainerData } from "../redux/containeroneslice";
const CustomForm = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getServerData({}));
  }, []);
  const [inputValue, setInputValue] = useState("");
  const {
    inputValue: originalValue,
    errorValue,
    ServerData,
  } = useSelector((state) => state.containerOne);
  console.log(ServerData);
  const handleClick = () => {
    dispatch(setContainerData({ inputValue }));
  };
  return (
    <>
      <input
        type="text"
        value={inputValue}
        onChange={(e) => {
          setInputValue(e.target.value);
        }}
      />
      <input type="button" value="submit" onClick={handleClick} />
      <div>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Title</th>
            </tr>
          </thead>
          <tbody>
            {ServerData.map((tableData, id) => {
              return (
                <tr key={id}>
                  <td>{tableData.id}</td>
                  <td>{tableData.title}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default CustomForm;
