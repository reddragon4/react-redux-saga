import { createSlice } from "@reduxjs/toolkit";


const containerOneSlice=createSlice({
    name:"containerOne",
    initialState:{ 
        inputValue:"From Slice.Intial value",
        errorValue:"Not an error",
        ServerData:[]
    },
    reducers:{
        getContainerData(){},
        setContainerData(state,action){
            console.log(action)
            const {inputValue}=action.payload
            return {...state,inputValue}
        },
    getServerData(){
        
    },
    setServerData(state,action){
       console.log("action:::",action); 
       const {data}=action.payload;
       return {...state,ServerData:data}
    }
    },
})
export const {getContainerData,setContainerData,getServerData,setServerData}=containerOneSlice.actions;
export default containerOneSlice.reducer
