import {call,put} from 'redux-saga/effects';
import { requestDataOne } from './containeronerequest';
import { setServerData } from './containeroneslice';

export function* ContainerOneHandler(action){
    try{
        const response=yield call(requestDataOne)
           yield put(setServerData({...response}))
    }catch(err){
        console.log(err)
    }
}